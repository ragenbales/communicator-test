#include<wx/wx.h>
#include<wx/socket.h>
#include <wx/splitter.h>

#include <cstring>

#include "frontend.cpp"

#define SOCKET_ID 2137

/* message formats
logging in  0|login length|password length|login|password
sending message  1|group index|message legth|message
joining a group  2|group name length|group name
*/

class MainWindow;

class Input : public wxPanel
{
public:
	Input(wxPanel *parent);

	void OnSend(wxCommandEvent& event);

	wxPanel *par;

	wxTextCtrl *entry;
	wxButton *send_button;

	static const int ID_ENTRY=101;
	static const int ID_BUTTON=102;
};

class inputGroup : public wxPanel {
public:
    inputGroup (wxWindow* parent, textArea* _groupList, parentSupervisor* _supervisor) : wxPanel (parent), groupList (_groupList), supervisor (_supervisor) {
        entry = new wxTextCtrl(this,ID_ENTRY,
                wxEmptyString,wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
        submit_button = new wxButton(this,ID_BUTTON,wxT("Join/Create Group"));
        Connect(ID_ENTRY,wxEVT_TEXT_ENTER,wxCommandEventHandler(inputGroup::onSubmit));
        Connect(ID_BUTTON,wxEVT_BUTTON,wxCommandEventHandler(inputGroup::onSubmit));

        wxBoxSizer *box = new wxBoxSizer(wxVERTICAL);

        box->Add(entry,1,wxEXPAND|wxALL,3);
        box->Add(submit_button,0,wxEXPAND|wxALL,3);

        this->SetSizer(box);
    }

    void onSubmit (wxCommandEvent& event) {
        wxString groupName = entry->GetValue ();
        entry->Clear();
        if (groupName.empty()) {return;}
        groupList->appendMessage (groupName, wxFont (14,wxFONTFAMILY_DEFAULT, wxFONTSTYLE_ITALIC, wxFONTWEIGHT_BOLD));
        groupList->Show(0);
        groupList->Show(1);
        if (supervisor != NULL) {
            supervisor->submitClick(groupName);
        }
    }

    parentSupervisor* supervisor;
    textArea *groupList;
	wxTextCtrl *entry;
	wxButton *submit_button;

	static const int ID_ENTRY=101;
	static const int ID_BUTTON=102;

};


class LoadingDialog : public wxFrame
{
private:
	bool connected=false;
public:
	LoadingDialog(MainWindow *parent);

	MainWindow *window;

	wxPanel *panel;

	wxStaticText *text;
	wxButton *cancel_button;

	void SetConnected(bool value);

	void OnCancel(wxCommandEvent& event);
	void OnClose(wxCloseEvent&);

	static const int ID_BUTTON=201;

	wxDECLARE_EVENT_TABLE();
};

wxBEGIN_EVENT_TABLE(LoadingDialog,wxFrame)
	EVT_CLOSE(LoadingDialog::OnClose)
wxEND_EVENT_TABLE()

class LoginPrompt : public wxFrame
{
private:
	bool submitted=false;
public:
	LoginPrompt(MainWindow *parent);

	MainWindow *window;

	wxPanel *panel;

	wxStaticText *ip_text,*port_text,*login_text,*group_text;
	wxTextCtrl *ip_entry,*port_entry,*login_entry,*group_entry;
	wxButton *submit_button;

	void OnSubmit(wxCommandEvent& event);
	void OnClose(wxCloseEvent&);

	static const int ID_BUTTON=301;

	wxDECLARE_EVENT_TABLE();
};

wxBEGIN_EVENT_TABLE(LoginPrompt,wxFrame)
	EVT_CLOSE(LoginPrompt::OnClose)
wxEND_EVENT_TABLE()

class MainWindow : public wxFrame, parentSupervisor
{
private:
	std::vector<std::string> last_person;
public:
	MainWindow(const wxString& title);

	wxPanel *panel;
    wxSplitterWindow* splitter;
    wxPanel * leftSide;
    textArea *groupChoice;
    inputGroup * groupInput;
	std::vector<textArea*> text;
	Input *input;
    int activeGroup = -1;

	wxIPV4address addr;

	wxSocketClient* sock;
    std::string totalMessage;

	int PORT = 2137;
	std::string IP = "127.0.0.1"; 
	std::string login;
    std::string group = "general";

	LoginPrompt *login_prompt;
	LoadingDialog *loading_dialog;

    const int GetActiveIndex () const {return activeGroup;}

	void ConnectSocket();

	void OnClose(wxCloseEvent&);
    void OnSocketEvent (wxSocketEvent&);

	//std::string GetLastPerson(int i) { return last_person; }
    //void SetLastPerson(std::string _last_person) { last_person = _last_person; }

    virtual void signalClick(int clicked) {
        splitter->GetWindow2()->Show(0);
        splitter->ReplaceWindow (splitter->GetWindow2(), text[clicked]); //later change clicked+1 to clicked
        activeGroup = clicked;
        splitter->GetWindow2()->Show(1);
    }
    virtual void submitClick (wxString groupName) {
        text.push_back(new textArea (splitter, false));
        text.back()->Show(0);
        text.back()->appendMessage (groupName); //later remove
        last_person.push_back("");
        char joinInfo [1+sizeof(int)+groupName.size()]; //TODO add unicode support for group names
        joinInfo[0] = 2;
        *(int*)(joinInfo+1) = groupName.size();
        for(int i=0;i<groupName.size();i++)
            joinInfo[sizeof(int)+1+i]=groupName[i];
        sock->Write (joinInfo, sizeof(joinInfo));
    }
/*joining a group  2|group name length|group name*/

	wxDECLARE_EVENT_TABLE();
};

wxBEGIN_EVENT_TABLE(MainWindow,wxFrame)
	EVT_CLOSE(MainWindow::OnClose)
    EVT_SOCKET (SOCKET_ID, MainWindow::OnSocketEvent)
wxEND_EVENT_TABLE()

class App : public wxApp
{
public:
	virtual bool OnInit();
};

Input::Input(wxPanel *parent)
	:wxPanel(parent,wxID_ANY)
{
	par=parent;

	entry = new wxTextCtrl(this,ID_ENTRY,
			wxEmptyString,wxDefaultPosition,wxDefaultSize,wxTE_PROCESS_ENTER);
	send_button = new wxButton(this,ID_BUTTON,wxT("Send"));

	Connect(ID_ENTRY,wxEVT_TEXT_ENTER,wxCommandEventHandler(Input::OnSend));
	Connect(ID_BUTTON,wxEVT_BUTTON,wxCommandEventHandler(Input::OnSend));

	wxBoxSizer *box = new wxBoxSizer(wxHORIZONTAL);

	box->Add(entry,1,wxEXPAND|wxALL,3);
	box->Add(send_button,0,wxALL,3);

	this->SetSizer(box);

	entry->SetFocus();
}

void Input::OnSend(wxCommandEvent& WXUNUSED(event))
{
	MainWindow *window = (MainWindow*) par->GetParent();
    if (window->GetActiveIndex() < 0) {
        return;
    }

	entry->SetFocus();

	wxString buffer = entry->GetValue();
	entry->Clear();

    wxString currentLine;
    for (int i = 0; i < buffer.size(); i++) {
        if (buffer[i] != '\n') {
            currentLine += buffer[i];
        } else {
            
			const char* bufferUTF8 = currentLine.ToUTF8();
			char writeInfo [2*sizeof (int)+1+sizeof(bufferUTF8)];
			*writeInfo = 1;
			*(int*)(writeInfo+1) = window->activeGroup; //group
			*(int*)(writeInfo+1+sizeof(int)) = strlen(bufferUTF8); //length
			for(int i=0;i<sizeof(bufferUTF8);i++)
				writeInfo[2*sizeof(int)+1+i]=bufferUTF8[i];
			window->sock->Write(writeInfo, sizeof(writeInfo));
			//window->sock->Write(bufferUTF8,strlen(bufferUTF8)+1);
            currentLine.clear();
        }
    }
    if (!currentLine.empty()) {
            
        const char* bufferUTF8 = currentLine.ToUTF8();
        char writeInfo [2*sizeof (int)+1+strlen(bufferUTF8)];
        *writeInfo = 1;
        *(int*)(writeInfo+1) = window->activeGroup; //group
        *(int*)(writeInfo+1+sizeof(int)) = strlen(bufferUTF8); //length
		for(int i=0;i<strlen(bufferUTF8);i++)
			writeInfo[2*sizeof(int)+1+i]=bufferUTF8[i];
        window->sock->Write(writeInfo, sizeof(writeInfo));
        //window->sock->Write(bufferUTF8,strlen(bufferUTF8)+1);
        //text->appendMessage (currentLine, wxFont (12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    }
}

LoadingDialog::LoadingDialog(MainWindow *parent)
	:wxFrame(parent,wxID_ANY,"Loading",wxDefaultPosition,wxSize(300,200))
{
	window = parent;

	SetMinSize(wxSize(200,200));

	panel = new wxPanel(this,wxID_ANY);
	wxBoxSizer *box = new wxBoxSizer(wxVERTICAL);
	
	box->AddStretchSpacer(3);

	text = new wxStaticText(panel,wxID_ANY,"Connecting to server...");
	box->Add(text,0,wxALL|wxALIGN_CENTER,3);

	box->AddStretchSpacer(1);

	cancel_button = new wxButton(panel,ID_BUTTON,wxT("Cancel"));
	box->Add(cancel_button,0,wxALL|wxALIGN_CENTER,3);
	Connect(ID_BUTTON,wxEVT_BUTTON,wxCommandEventHandler(LoadingDialog::OnCancel));

	box->AddStretchSpacer(3);

	panel->SetSizer(box);
}

void LoadingDialog::SetConnected(bool value)
{
	connected = value;
}

void LoadingDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	this->Close();
}

void LoadingDialog::OnClose(wxCloseEvent& e)
{
	if(connected)
	{
		window->Show(true);
	}
	else
	{
		window->Close(true);
	}
    e.Skip();
}

LoginPrompt::LoginPrompt(MainWindow *parent)
	:wxFrame(parent,wxID_ANY,"Login",wxDefaultPosition,wxSize(400,500))
{
	window = parent;

	SetMinSize(wxSize(300,300));

	panel = new wxPanel(this,wxID_ANY);
	wxBoxSizer *box = new wxBoxSizer(wxVERTICAL);

	box->AddStretchSpacer(1);
	
	ip_text = new wxStaticText(panel,wxID_ANY,"Server IP address:");
	box->Add(ip_text,0,wxALIGN_CENTER|wxALL,3);

	ip_entry = new wxTextCtrl(panel,wxID_ANY,window->IP,
			wxDefaultPosition,wxSize(300,-1));
	box->Add(ip_entry,0,wxALIGN_CENTER|wxALL,3);

	box->AddStretchSpacer(1);
	
	port_text = new wxStaticText(panel,wxID_ANY,"Port:");
	box->Add(port_text,0,wxALIGN_CENTER|wxALL,3);

	port_entry = new wxTextCtrl(panel,wxID_ANY,std::to_string(window->PORT),
			wxDefaultPosition,wxSize(300,-1));
	box->Add(port_entry,0,wxALIGN_CENTER|wxALL,3);

	box->AddStretchSpacer(1);

	group_text = new wxStaticText(panel,wxID_ANY,"Name of the user or group you want to talk to:");
	box->Add(group_text,0,wxALIGN_CENTER|wxALL,3);

	group_entry = new wxTextCtrl(panel,wxID_ANY,window->group,
			wxDefaultPosition,wxSize(300,-1));
	box->Add(group_entry,0,wxALIGN_CENTER|wxALL,3);

	box->AddStretchSpacer(1);

	login_text = new wxStaticText(panel,wxID_ANY,"Login:");
	box->Add(login_text,0,wxALIGN_CENTER|wxALL,3);

	login_entry = new wxTextCtrl(panel,wxID_ANY,window->login,
			wxDefaultPosition,wxSize(300,-1));
	box->Add(login_entry,0,wxALIGN_CENTER|wxALL,3);
	login_entry->SetFocus();

	box->AddStretchSpacer(1);

	submit_button = new wxButton(panel,ID_BUTTON,wxT("Connect"));
	box->Add(submit_button,0,wxALIGN_CENTER|wxALL,3);
	Connect(ID_BUTTON,wxEVT_BUTTON,wxCommandEventHandler(LoginPrompt::OnSubmit));

	box->AddStretchSpacer(1);

	panel->SetSizer(box);
}

void LoginPrompt::OnSubmit(wxCommandEvent& WXUNUSED(event))
{
	submitted = true;

	window->IP = ip_entry->GetValue(); 
	window->PORT = stoi( (std::string) port_entry->GetValue() );
	window->login = login_entry->GetValue(); 
	window->group = group_entry->GetValue();

	this->Close();
}

void LoginPrompt::OnClose(wxCloseEvent& e)
{
	if(submitted)
	{
		window->ConnectSocket();
	}
	else
	{
		window->Close();
	}
    e.Skip();
}

MainWindow::MainWindow(const wxString& title)
	:wxFrame(NULL,wxID_ANY,title,wxDefaultPosition,wxSize(400,500))
{
	SetMinSize(wxSize(300,300));

	panel = new wxPanel(this,wxID_ANY, wxDefaultPosition, wxDefaultSize);
	
    splitter = new wxSplitterWindow (panel, wxID_ANY, wxDefaultPosition, wxDefaultSize);
    leftSide = new wxPanel (splitter);
    groupChoice = new textArea (leftSide, true, this);
    groupInput = new inputGroup (leftSide, groupChoice, this);
    //groupChoice->SetMinSize (wxSize(100,300));
    //text.push_back(new textArea (splitter, false));
    //text->SetMinSize (wxSize (200, 300));
    splitter->SetMinimumPaneSize (10);
    //splitter->SetMinimumPaneSize (100); this doesnt work i have no idea why. It affects the initial position of sash for some reason
    splitter->SplitVertically(leftSide, new textArea (splitter, false));
    //new textArea (splitter, false)
    splitter->SetSashGravity (0.25);
	input = new Input(panel);

	wxBoxSizer *box = new wxBoxSizer(wxVERTICAL);

	box->Add(splitter,1,wxEXPAND|wxALL,3);
	box->Add(input,0,wxEXPAND|wxALL,3);

	panel->SetSizer(box);

    wxBoxSizer *boxLeft = new wxBoxSizer (wxVERTICAL);
	boxLeft->Add(groupChoice,1,wxEXPAND|wxALL,3);
	boxLeft->Add(groupInput,0,wxEXPAND|wxALL,3);
    leftSide->SetSizer(boxLeft);


	login_prompt = new LoginPrompt(this);
	loading_dialog = new LoadingDialog(this);
}

void MainWindow::ConnectSocket()
{
	addr.Hostname(IP);
	addr.Service(PORT);

    sock = new wxSocketClient();
    sock->SetEventHandler (*this, SOCKET_ID);
    sock->SetNotify (wxSOCKET_CONNECTION_FLAG | wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
    sock->Notify(1);

	loading_dialog->Show(true);

	sock->Connect(addr,false);
}

void MainWindow::OnClose(wxCloseEvent& e)
{
	sock->Shutdown();
    e.Skip();
}

void MainWindow::OnSocketEvent (wxSocketEvent& e) {
    wxSocketBase* sock = e.GetSocket();
    switch (e.GetSocketEvent()) {
        case wxSOCKET_INPUT:
        {
            char type;
            sock->Read (&type, 1);
            switch (type) {
            case 1:
                int readInfo [3];
                sock->Read(readInfo, 3*sizeof(int)); //group is readInfo [0], senderSize = readInfo[1], messageSize = readInfo[2]
                char buffer [readInfo[1]+readInfo[2]+1];
                sock->Read(buffer,sizeof(buffer)-1);
                buffer [sizeof(buffer)-1] = 0;

                totalMessage = buffer;


                std::string sender (buffer, readInfo[1]),message (buffer+readInfo[1], readInfo[2]);


                if (sender!=last_person[readInfo[0]]) {
                    text[readInfo[0]]->appendMessage (sender, wxFont (14,wxFONTFAMILY_DEFAULT, wxFONTSTYLE_ITALIC, wxFONTWEIGHT_BOLD)); //text[0] -> text[readInfo[0]
                    //SetLastPerson (sender);
                    last_person[readInfo[0]] = sender;
                }
                text[readInfo[0]]->appendMessage (wxString(message.c_str(),wxConvUTF8), wxFont (12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
                if (text[readInfo[0]]->getIsAtBottom()) {
                    text[readInfo[0]]->scrollToBottom();
                }

                if (readInfo[0] == activeGroup) {
                    text[readInfo[0]]->Show(0);
                    text[readInfo[0]]->Show(1); //stupid workaround cause i can't find how to force a window to repaint
                }
                
                break;
            }

            break;
        }
		case wxSOCKET_CONNECTION:
		{
			loading_dialog->SetConnected(true);
			loading_dialog->Close();

			//give server login and group
			//const char* buffer_login_group = (login+'|'+group).c_str();
			char loginInfo [2*sizeof(int)+1+login.size()];
            *loginInfo = 0;
            *(int*)(loginInfo+1) = login.size(); //login length
            *(int*)(loginInfo+1+sizeof(int)) = 0; //password length
			for(int i=0;i<login.size();i++)
				loginInfo[2*sizeof(int)+1+i]=login[i];
            sock->Write (loginInfo, sizeof(loginInfo));
            //sock->Write(login.c_str(),login.size());

			break;
		}
        case wxSOCKET_LOST:
        {
            sock->Destroy();
            //wxPostEvent (this,wxCloseEvent());
			this->Close();
            break;
        }
    }
}

bool App::OnInit()
{
	MainWindow *window = new MainWindow(wxT("Communicator"));
	window->login_prompt->Show(true);

	return true;
}

IMPLEMENT_APP(App);
