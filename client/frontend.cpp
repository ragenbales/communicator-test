#include "wx/wx.h"

class parentSupervisor {
public:
    virtual void signalClick(int) {}
    virtual void submitClick(wxString) {}
};

class line {
private:
    static constexpr int borderH = 5, borderV = 8; //borders drawn around text
    wxString string; // content of the line
    wxFont font; //font used for drawing
    std::vector<int> lineLengths; //numbers of characters written before \n. The lineLengths[i] is actually the sum of line lengths to i
    wxBrush innerBrush, outerBrush; //brushes for drawin around text

public:
    line (wxString _string, wxFont _font, wxBrush _innerBrush = wxBrush (wxColour(200,200,200)), wxBrush _outerBrush = wxBrush(wxColour(100,100,100))) :string (_string), font (_font), innerBrush (_innerBrush), outerBrush (_outerBrush){}
    
    line () :lineLengths ({0}) {}
    void set (wxString _string, wxFont _font, wxBrush _innerBrush = wxBrush (wxColour(200,200,200)), wxBrush _outerBrush = wxBrush(wxColour(100,100,100))) {
        string = _string; 
        font = _font; 
        innerBrush = _innerBrush;
        outerBrush = _outerBrush;
    }
    void setBrushes (wxBrush _innerBrush = wxBrush (wxColour(200,200,200)), wxBrush _outerBrush = wxBrush(wxColour(100,100,100))) {
        innerBrush = _innerBrush;
        outerBrush = _outerBrush;
    }

    int recalculateLengths (wxDC* dc, int width) { //function used for recalculating lineLengths, used after a resize
    
        wxFont previousFont = dc->GetFont(); //its always nice to clean up after yourself
        dc->SetFont (font);

        width -= 2*borderH;
        lineLengths.clear();
        lineLengths.push_back(0);
        int currentBeginning = 0;

        int averageLineLength = width/dc->GetCharWidth();  //used for guessing the first checked length in binary search

        while (currentBeginning < (int)string.size()) { 
            int maximumLength = 0;
            
            for (int step = averageLineLength / 2; step > 0; step /= 2) { //this is binary search. maximumLength is the length of the current fragment, 
                //step is the number of characters that it tries to add to maximumLength, and currentBeginning is the start of current line
                while (currentBeginning + maximumLength + step <= (int)string.size() && dc->GetTextExtent (wxString (string, currentBeginning, maximumLength+step)).GetWidth() < width) {
                    maximumLength += step;
                }
            }
            if (maximumLength == 0) {
                lineLengths.push_back (1+lineLengths.back());
                currentBeginning += 1;
            } else {
                lineLengths.push_back (maximumLength+lineLengths.back());
                currentBeginning += maximumLength;
            }
        }

        int lineHeight;
        dc->GetTextExtent (_T("GetTAaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"),NULL,&lineHeight);

        dc->SetFont (previousFont);
        return (lineLengths.size()-1) * lineHeight+2*borderV; //return the height of drawn text, for the ease of drawing
    }

    int drawFromTop (wxDC* dc, int x, int highestY, int width, int topY, int bottomY) { 
        
        wxFont previousFont = dc->GetFont();  //used to set previous font and brush after the operations
        wxBrush previousBrush = dc->GetBrush();

        int lineHeight;
        dc->SetFont (font);
        dc->GetTextExtent (_T("GetTAaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"),NULL,&lineHeight);

        if (highestY > bottomY || highestY+((int)lineLengths.size()-1) * lineHeight + 2*borderV < topY) { //if the text is entirely not drawn, return
            return (lineLengths.size()-1) * lineHeight +2*borderV;
        }
        
        dc->SetBrush (outerBrush); //draw colorful borders
        dc->DrawRectangle (x, highestY, width, (lineLengths.size()-1) * lineHeight + 2*borderV);
        dc->SetBrush (innerBrush);
        dc->DrawRectangle (x+borderH/2, highestY+borderV/2, width-borderH, (lineLengths.size()-1) * lineHeight + borderV);
        highestY += borderV;


        int skippedLines = std::max (0, (topY-highestY)/lineHeight); //calculate how many lines are out of the window, so the program doesn't need to draw them

        int currentY = highestY+skippedLines*lineHeight;
        int currentBeginning = lineLengths[skippedLines];
        for (int i = skippedLines; i < (int)lineLengths.size()-1 && currentBeginning < (int)string.size(); i++) {
            if (currentY < bottomY && currentY + lineHeight > topY) {
                dc->DrawText (wxString(string,currentBeginning,lineLengths[i+1]-lineLengths[i]), x+borderH, currentY); //draw the line
            }
            currentY += lineHeight;
            currentBeginning += lineLengths[i+1]-lineLengths[i]; //calculate where to draw next line
        }

        dc->SetBrush (previousBrush);
        dc->SetFont (previousFont);
        return (lineLengths.size()-1) * lineHeight + 2*borderV;
    }
};

class textArea : public wxScrolledWindow {
private:
    static constexpr int scrollRate = 8; //arbitrary number used to set how fast to scroll
    bool isAtBottom; //if the user is at bottom, program should scroll when gets new messages
    double scrolledFragment; //where is the user in the messages, double from 0 to 1
    static constexpr int inf = 2147483647; //maximum int value, used to scroll all the way down
    std::vector<line> contents; //Lines to be drawn
    std::vector<int> positions; //used to calculate which messages to draw faster
    int currentActive = -1;
    bool isClickable;
    parentSupervisor * parentToNotify;
    int id; //remove later
public:
    int& getId () {return id;}
    textArea (wxWindow* frame, bool _isClickable, parentSupervisor* _parentToNotify = NULL) : wxScrolledWindow (frame, wxID_ANY), isClickable (_isClickable), parentToNotify (_parentToNotify) {
        scrolledFragment = 1;
        isAtBottom = 1;
        SetScrollRate (0,scrollRate);
        positions.push_back(0);
    }
    void appendMessage (wxString str, wxFont f = wxFont (12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL)) { 
        contents.emplace_back(str, f);
        wxWindowDC dc (this);

        int width;
        GetSize (&width, NULL);
        int appendedHeight = contents.back().recalculateLengths (&dc, width);
        positions.push_back (positions.back()+appendedHeight); //calculate the width and height of the new message
        SetVirtualSize (0,positions.back()); //used to let the scrollbar know how much it can scroll
        if (isAtBottom) {
            scrollToBottom(); 
        }
    }
    void scrollToBottom () {
        Scroll (0,inf);
    }
    const bool getIsAtBottom () const {return isAtBottom;}
    void OnPaint(wxPaintEvent&);
    void OnSize (wxSizeEvent&);
    void OnScroll (wxScrollWinEvent& e);
    bool SetActive (int newActive) {
        if (currentActive != -1) {
            contents[currentActive].setBrushes();
        }
        contents[newActive].setBrushes(wxBrush(wxColour(0,0,200)), wxBrush (wxColour (0,0,100)));
        bool ret = currentActive != newActive;
        currentActive = newActive;
        return ret;
    }
    void OnClick (wxMouseEvent& e) {
        
        wxWindowDC dc (this);
        DoPrepareDC(dc);
        wxPoint pt = e.GetLogicalPosition (dc);
        if (pt.y > positions.back()) {
            return;
        }
        int index = (std::upper_bound(positions.begin(),positions.end(),pt.y) - positions.begin())-1;
        bool toCallMother;
        if (isClickable) {
            toCallMother = SetActive(index);   
        }
        Show(0);
        Show(1);
        if (parentToNotify != NULL && toCallMother) {
            parentToNotify->signalClick(index);
        }

    }
    DECLARE_EVENT_TABLE()
};


void textArea::OnScroll (wxScrollWinEvent& e) {
    int h1, h2, h3;
    GetVirtualSize (NULL, &h1);
    GetClientSize (NULL, &h3);
    GetViewStart (NULL, &h2);
    h1 -= h3; //h1 is the maximum scroll position
    h2 *= scrollRate; //h2 is the current scroll position
    if (h1 != 0) {
        scrolledFragment = double(h2)/h1;
    } else {
        scrolledFragment = 1;
    } // calculate where the view is
    if (h1 - h2 < 30) { //just a margin of error
        isAtBottom = 1;
    } else {
        isAtBottom = 0;
    }
    e.Skip();
}

void textArea::OnSize (wxSizeEvent& e) {
    wxWindowDC dc (this);
    positions.clear();
    positions.push_back(0);
    for (int i = 0; i < (int)contents.size(); i++) {
        positions.push_back(positions.back()+contents[i].recalculateLengths (&dc, e.GetSize().GetWidth())); //recalculate heights of all messages
    }
    SetVirtualSize (0,positions.back()); //let the scrollbar know how much it can scroll

    int h1, h2, h3;
    GetVirtualSize (NULL, &h1);
    GetClientSize (NULL, &h3);
    GetViewStart (NULL, &h2);
    h1 -= h3; //h1 is the maximum scroll position
    h2 *= scrollRate; //h2 is the currentscroll position
    scrolledFragment = double(h2)/h1;
    Scroll (0, scrolledFragment*h1); //scroll to adequate position
    if (isAtBottom) {
        scrollToBottom();
    }
    Show(0);
    Show(1);
    e.Skip();
}

void textArea::OnPaint (wxPaintEvent& e) {
    wxPaintDC dc (this);
    DoPrepareDC(dc);
    int width;
    int topY;
    GetViewStart (NULL, &topY);
    int bottomY;
    GetSize (&width, &bottomY);
    topY *= scrollRate; //calculate the y value of top of the client area
    bottomY += topY; //calculate the y value of the bottom of the client area
    int firstDrawn = (std::upper_bound(positions.begin(), positions.end(), topY) - positions.begin()); //calculate with binary search which message to draw first
    firstDrawn--;
    int currentY = positions[firstDrawn];
    for (; firstDrawn < (int)contents.size(); firstDrawn++) {
        currentY += contents[firstDrawn].drawFromTop(&dc, 0, currentY, width, topY, bottomY); 
        if (currentY >= bottomY) break;
    }
    e.Skip();
}

BEGIN_EVENT_TABLE (textArea, wxWindow)
    EVT_SCROLLWIN (textArea::OnScroll)
    EVT_PAINT (textArea::OnPaint)
    EVT_SIZE (textArea::OnSize)
    EVT_LEFT_DOWN (textArea::OnClick)
END_EVENT_TABLE ()
