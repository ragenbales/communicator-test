#include <iostream>
#include <string>
#include <gmpxx.h>

class bigNum {
private:
    mpz_t number;
    static gmp_randstate_t state;
public:
    bigNum () {
        mpz_init(number);
    }
    bigNum (long long l) {
        mpz_init(number);
        mpz_set_si (number, l);
    }
    bigNum (const std::string& str) {
        mpz_init(number);
        mpz_set_str (number, str.c_str(), 10);
    }
    bigNum (const bigNum& b) {
        mpz_init(number);
        mpz_set (number, b.number);
    }
    const bigNum& operator = (long long l) {
        mpz_set_si (number, l);
        return *this;
    }
    const bigNum& operator = (const std::string &str) {
        mpz_set_str (number, str.c_str(), 10);
        return *this;
    }
    const bigNum& operator = (const bigNum& b) {
        mpz_set (number, b.number);
        return *this;
    }
    const bigNum& operator += (const bigNum& b) {
        mpz_add (number, number, b.number);
        return *this;
    }
    const bigNum& operator *= (const bigNum& b) {
        mpz_mul (number, number, b.number);
        return *this;
    }
    const bigNum& operator -= (const bigNum& b) {
        mpz_sub (number, number, b.number);
        return *this;
    }
    const bigNum& operator /= (const bigNum& b) {
        mpz_tdiv_q (number, number, b.number);
        return *this;
    }
    const bigNum& operator %= (const bigNum& b) {
        mpz_mod (number, number, b.number);
        return *this;
    }
    friend std::istream& operator >> (std::istream& i, bigNum& b);
    friend std::ostream& operator << (std::ostream& o, const bigNum& b);
    friend void bigNumRandomInit (const bigNum& b);
    bool operator == (const bigNum& b) const {
        return (mpz_cmp(number, b.number) == 0);
    }
    bool operator >= (const bigNum& b) const {
        return (mpz_cmp(number, b.number) <= 0);
    }
    bool operator <= (const bigNum& b) const {
        return (mpz_cmp(number, b.number) <= 0);
    }
    bool operator > (const bigNum& b) const {
        return (mpz_cmp(number, b.number) > 0);
    }
    bool operator < (const bigNum& b) const {
        return (mpz_cmp(number, b.number) < 0);
    }
    void power (const bigNum& pow, const bigNum& mod) {
        mpz_powm (number, number, pow.number, mod.number);
    }
    bool probablyPrime () {
        return mpz_probab_prime_p (number, 40);
    }
    void randomiseBits(long long bits) {
        mpz_urandomb (number, state, bits);
    }
    void randomiseNum(const bigNum& b) {
        mpz_urandomm (number, state, b.number);
    }
    void nextPrime () {
        mpz_nextprime(number, number);
    }
    void randomPrime(long long bits) {
        randomiseBits(bits);
        nextPrime();
    }
    void setTo2Pow (long long exp) {
        mpz_set_ui (number, 1);
        mpz_mul_2exp (number, number, exp);
    }
};

gmp_randstate_t bigNum::state;

bigNum operator + (bigNum l, const bigNum& r) {
    l += r;
    return l;
}
bigNum operator - (bigNum l, const bigNum& r) {
    l -= r;
    return l;
}
bigNum operator * (bigNum l, const bigNum& r) {
    l *= r;
    return l;
}
bigNum operator / (bigNum l, const bigNum& r) {
    l /= r;
    return l;
}
bigNum operator % (bigNum l, const bigNum& r) {
    l %= r;
    return l;
}

std::istream& operator >> (std::istream& i, bigNum& b) {
    std::string str;
    i >> str;
    b = str;
    return i;
}

std::ostream& operator << (std::ostream& o, const bigNum& b) {
    std::string str (mpz_get_str(NULL,10,b.number));
    o << str;
    return o;
}

void bigNumRandomInit (const bigNum& b) {
    gmp_randinit_mt (bigNum::state);
    gmp_randseed (bigNum::state, b.number);
}


bigNum p;
bigNum A;
bigNum order;


void check (const bigNum& x, const bigNum& y) {
    bigNum l = y*y;
    bigNum r = x;
    r += A;
    r *= x;
    r += 1;
    r *= x;
    bigNum tmp = l-r;
    std::cout << "l-r (should be 0) " << (tmp%p) << std::endl;
}

void makeRandomPrivateKey (bigNum& b) {
    b.randomiseNum (order);
}

void rootModN (bigNum& b) { //assuming p is 5 mod 8
    bigNum tmp (b);
    tmp.power ((p-1)/4, p);
    bigNum k = p;
    k -= 5;
    k /= 8;
    if (tmp == 1) {
        b.power (k+1, p);
    } else {
        b.power (k+1, p);
        bigNum tP = 2;
        tP.power (k*2+1, p);
        b *= tP;
        b %= p;
    }
}

void setUp (bigNum in, bigNum& x, bigNum& y) {
    x = in;
    y = x;
    y += A;
    y *= x;
    y += 1;
    y *= x;
    rootModN (y);
}

void add (const bigNum px, const bigNum py, const bigNum qx, const bigNum qy, bigNum& rx, bigNum& ry) {
    if (px == -1) {
        rx = qx;
        ry = qy;
        return;
    }
    if (qx == -1) {
        rx = px;
        ry = py;
        return;
    }
    if (px == qx && py+qy == 0) {
        rx = -1;
        ry = 0;
        return;
    }
    bigNum lambda;
    if (px == qx && py == qy) {
        lambda = px;
        lambda.power (2,p);
        lambda *= 3;
        lambda += A*2*px;
        lambda += 1;
        bigNum tmp = py*2;
        tmp.power (-1,p);
        lambda *= tmp;
        lambda %= p;
    } else {
        lambda = qy-py;
        bigNum tmp = qx-px;
        tmp.power (-1, p);
        lambda *= tmp;
        lambda %= p;
    }
    rx = lambda*lambda-A-px-qx;
    ry = py + lambda*(rx-px);
    rx %= p;
    rx += p;
    rx %= p;
    ry %= p;
    ry += p;
    ry %= p;
}

void pow (bigNum x, bigNum y, bigNum& ox, bigNum& oy, bigNum power) {
    ox = -1;
    oy = 0;
    while (power > 0) {
        if (power % 2 == 1) {
            add (ox,oy,x,y,ox,oy);
        }
        add (x,y,x,y,x,y);
        power /= 2;
    }
}

void makePublicKey (bigNum& privKey, bigNum& x) {
    bigNum y;
    setUp (9,x,y);
    pow (x,y,x,y,privKey);
}

void makeSymmetricKey (const bigNum& priv, const bigNum& pub, bigNum& sym) {
    bigNum x,y;
    setUp (pub,x,y);
    pow (x,y,sym,y,priv);
}

void initPrime () {
    p.setTo2Pow (255);
    p -= 19;
    A = 486662;
    order.setTo2Pow (252);
    order += bigNum("27742317777372353535851937790883648493");
}

int main() {
    bigNumRandomInit(2); // seed prng. chosen by a fair dice roll, guaranteed to be random
    initPrime();
    bigNum privB;
    bigNum privC;
    makeRandomPrivateKey (privB);
    makeRandomPrivateKey (privC);
    bigNum pubB, pubC;
    makePublicKey (privB, pubB);
    makePublicKey (privC, pubC);
    bigNum symB, symC;
    makeSymmetricKey (privB, pubC, symB);
    makeSymmetricKey (privC, pubB, symC);
    std::cout << symB << std::endl;
    std::cout << symC << std::endl;

}
