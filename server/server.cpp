#include<iostream>
#include<fstream>
#include<string.h>

#include<set>
#include<map>
#include<vector>

#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>

#include "csv.h"

class Client;

class Message
{
/* TYPES:
   -1 - no ongoing message
   0 - login (type|login lenght|password lenght|login|password)
   1 - message (type|group index|message lenght|message)
   2 - group join (type|gid length|gid)
*/
private:
	int type;

    //login data
	int left_login;
	int left_password;
	std::string login;
	std::string password;

    //message data
	int group_index;
	int left_message;
	std::string message;

    //group join data
    int left_gid;
    std::string gid;
public:
	Message() { type = -1; }

	int GetType() { return type; };
	int GetLeft();

	void Recieve(char* msg,int len,Client& cli);
	void RecieveNew(char* msg,int len,Client& cli);

	void Commit(Client& cli);
};

class Client // class representing a client connected to the server
{
private:
	int socket; // file descriptor of the client's socket
	int uid; // (user id) id of the client
	std::vector<std::string> gids; // (group ids) ids of groups the client is connected to
	std::map<std::string,int> group_index; // what index group of a given id has in the `gids` vector
	std::string name; // client's username
	bool logged_in;
public:
	Client(int _socket=-1,int _uid=-1) // basic constructor, assigning needed variables
	{
		socket=_socket;
		uid=_uid;
		logged_in = false;
	}

	Message message;

	int GetSocket() { return socket; } // returns client's socket file descriptor
	int GetUID() { return uid; } // returns client's id
	std::string GetGID(int index); // returns id of the group client is connected to or -1 if doesnt exist
	int GetGroupIndex(std::string); // returns index of the given group in the gids vector or -1 if doesnt exist
	std::string GetName() { return name; } // returns client username

	void LogIn(std::string login,std::string password);

	void AddGroup(std::string gid);

	void ReadOldMessages(std::string gid); // sends client all messages previously written in the group they connected to
};

class Group // class representing a group at least one client is currently connected to
{
private:
	std::string gid; // id of the group
	//std::string name; // name of the group
	std::set<int> users; // set containing ids of clients currently connected to this group
	std::string file_name; // name of the csv file containing messages written in this group
public:
	Group(std::string _gid="") // basic constructor, assigning needed variables
	{
		gid=_gid;
		//name=_name;
		file_name = "db/"+gid+".messages";
	}

	std::string GetGID() { return gid; } // returns id of the group
	//std::string GetName() { return name; } // returns name of the group
	std::string GetFileName() { return file_name; } // returns name of the csv file where messages from this group are written
	std::set<int> GetUsers() { return users; } // returns set of ids of clients currently connected to the group
	void AddUser(int uid) { users.insert(uid); } // adds a new connected client to the group
	void RemoveUser(int uid) { users.erase(uid); } // marks a client as disconnected from the group

	void AddMessage(std::string sender,std::string message); // adds a new message to the csv file for this group and sends to connected clients
};

const int PORT=2137; // default port for the server socket

int next_uid=0; // next free id for clients
//int next_gid=0; // next free id for groups

std::map<int,Client> clients; // map of clients currently connected to the server ( id -> client )
std::map<std::string,Group> groups; // map of groups clients are currently connected to ( id -> group )

//std::map<std::string,int> group_id; // map containing ids of currently existing groups ( name -> id )

const int buffer_size = 255;
char buffer[buffer_size]; // buffer for reading from the sockets and its size

int Message::GetLeft()
{
	switch( type )
	{
		case 0:
			/*if( left_login == 0 )
				return left_password;
			return left_login;*/
            return left_login+left_password;
		case 1:
			return left_message;
        case 2:
            return left_gid;
	}
	return 0;
}

void Message::Recieve(char* msg,int len,Client& cli)
{
	char* msg_end = msg+len;

	switch ( type )
	{
		case 0:
			while( msg != msg_end && left_login > 0 )
			{
				login += *msg;
				msg++;
                left_login--;
			}
			while( msg != msg_end && left_password > 0 )
			{
				password += *msg;
				msg++;
                left_password--;
			}
            break;
		case 1:
			while( msg != msg_end && left_message > 0 )
			{
				message += *msg;
				msg++;
                left_message--;
			}
            break;
        case 2:
            while (msg != msg_end && left_gid > 0) {
                gid += *msg;
                msg++;
                left_gid--;
            }
            break;
	}

	if( GetLeft() <= 0 )
	{
		Commit(cli);
	}
}

void Message::RecieveNew(char* msg,int len,Client& cli)
{
	type = msg[0];

	switch ( type )
	{
		case 0:
            {
                if( len < 1+2*(int)sizeof(int) )
                {
                    type = -1;
                    break;
                }
                left_login = *(int*)(msg+1);
                left_password = *(int*)(msg+1+sizeof(int));
                Recieve(msg+1+2*sizeof(int),len-1-2*sizeof(int),cli);
            }
            break;
		case 1:
            {
                if( len < 1+2*(int)sizeof(int) )
                {
                    type = -1;
                    break;
                }
                group_index = *(int*)(msg+1);
                left_message = *(int*)(msg+1+sizeof(int));
                Recieve(msg+1+2*sizeof(int),len-1-2*sizeof(int),cli);
            }
            break;
        case 2:
            {
                if( len < 1+(int)sizeof(int) )
                {
                    type = -1;
                    break;
                }
                left_gid = *(int*)(msg+1);
                Recieve (msg+1+sizeof(int),len-1-sizeof(int), cli);
            }
            break;
	}
}

void Message::Commit(Client& cli)
{
	switch ( type )
	{
		case 0:
			{
                cli.LogIn(login,password);
                login.clear();
                password.clear();
            }
            break;
		case 1:
            {
                std::string messageGid = cli.GetGID(group_index);
                if( messageGid == "" )
                {
                    break;
                }
                groups[messageGid].AddMessage(cli.GetName(),message);
                message.clear();
            }
            break;
        case 2:
            {
                if (groups[gid].GetGID() == "") {
                    groups[gid] = Group (gid);
                }
                cli.AddGroup (gid);
                groups[gid].AddUser (cli.GetUID());
                gid.clear();
            }
            break;
	}
}

std::string Client::GetGID(int index)
{
	if( index >= gids.size() )
		return "";
	return gids[index];
}

int Client::GetGroupIndex(std::string gid)
{
	if(group_index.find(gid) == group_index.end())
    	return -1;
	return group_index[gid];
}

void Client::LogIn(std::string login,std::string password)
{
	name = login;
	logged_in = true;

//TEMPORARY add to group 0
	//groups[0].AddUser(uid);
	//AddGroup(0);

	//ReadOldMessages();
}

void Client::AddGroup(std::string gid)
{
	gids.push_back(gid);
	group_index[gid] = gids.size()-1;
    ReadOldMessages(gid);
}

void Client::ReadOldMessages(std::string gid)
{
//TEMPORARY only reading group 0
	std::vector<std::string> messages = read_conversation( groups[gid].GetFileName() );

	/*for(auto msg:messages)
	{
		char to_send[msg.size()+1+sizeof(int)];
		for(size_t i=0;i<msg.size();i++)
			to_send[i+1+sizeof(int)]=msg[i];

        to_send[0] = 1;
		*(int*)(to_send+1) = GetGroupIndex(gid); // *(int*)(to_send+1) = GetGroupIndex(*(int*)(to_send+1));

		send(GetSocket(),to_send,sizeof(to_send),0);
	}*/

	std::string long_message;

	for(auto msg:messages) {
        char info [1+sizeof(int)];
        info[0] = 1;
        *(int*)(info+1) = GetGroupIndex(gid);
        long_message+=std::string(info, info+sizeof(info));
		long_message+=msg;
    }

	/*char buffer_long[long_message.size()+1];
	strcpy(buffer_long,long_message.c_str());
	buffer_long[long_message.size()] = 0;

	send(GetSocket(),buffer_long,long_message.size()+1,0);
    */
    send(GetSocket(),long_message.c_str(),long_message.size(),0);
}

void Group::AddMessage(std::string sender,std::string message)
{
	std::string to_send = sender+message;

	for(auto reciever:GetUsers())
	{
		char info[1+3*sizeof(int)];
		info[0]=1;
		*(int*)(info+1) = clients[reciever].GetGroupIndex(gid); //*(int*)(info+1) = clients[reciever].GetGroupIndex(gid);
		*(int*)(info+1+sizeof(int)) = sender.size();
		*(int*)(info+1+2*sizeof(int)) = message.size();

		send(clients[reciever].GetSocket(),info,sizeof(info),0);
		send(clients[reciever].GetSocket(),to_send.c_str(),to_send.size(),0);
	}

    /*
	char info[1+3*sizeof(int)];
	info[0]=1;
	*(int*)(info+1) = gid;
	*(int*)(info+1) = (int)sender.size();
	*(int*)(info+1+*sizeof(int)) = (int)message.size();
    */
	//add_message(GetFileName(),std::string(info,info+1+3*sizeof(int))+to_send); //TODO fix history support
    char info [2*sizeof(int)];
    *(int*)(info) = (int)sender.size();
    *(int*)(info+sizeof(int)) = (int)message.size();
    add_message(GetFileName(), std::string(info,info+sizeof(info))+to_send);
}

int main()
{
//TEMPORARY creating group 0 where everyone belongs
	//groups[0]=Group(0,"general");
	//group_id["general"]=0;

	// Create a socket
	/* arguments: 
		domain (AF_INET for IPv4), 
		type (SOCK_STREAM provides two-ways byte streams), 
		protocol (0 if only one exists) */
	int sock=socket(AF_INET,SOCK_STREAM,0);
	if( sock == -1 )
	{
		perror("Unable to create socket");
		exit(EXIT_FAILURE);
	}

	// Modify socket options
	/* arguments:
		socket,
		level of manipulation (SOL_SOCKET for API level options),
		options (SO_REUSEADDR|SO_REUSEPORT to make IP and port availible for rebinding right after the socket closes)
		value,
		size of value */
	int opt=1;
	int setsockopt_error=setsockopt(sock,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&opt,sizeof(opt));
	if( setsockopt_error == -1 )
	{
		perror("Error encountered while modifying socket options");
		exit(EXIT_FAILURE);
	}

	// Structure containing server address
	/* values:
		family/domain (AF_INET for IPv4),
		port (htons() converts form network to host byte order),
		address (ntohl() converts host to network byte order) */
	struct sockaddr_in address;
	address.sin_family=AF_INET;
	address.sin_port=htons(PORT);
	address.sin_addr.s_addr=INADDR_ANY;

	// Bind the socket to an address
	/* arguments:
		socket,
		address (in sockaddr structure),
		size of address */
	int bind_error=bind(sock,(struct sockaddr *)&address,sizeof(address));
	if( bind_error == -1 )
	{
		perror("Unable to bind socket");
		exit(EXIT_FAILURE);
	}

	// Listen for connection requests from clients
	/* arguments:
		socket,
		maximal queue size */
	int listen_error=listen(sock,3);
	if( listen_error == -1 )
	{
		perror("Error ecountered while listening for connection requests");
		exit(EXIT_FAILURE);
	}

	while(true)
	{
		// creating set of file descriptors and clearing it
		fd_set fd;
		FD_ZERO(&fd);

		// adding server socket to the set to listen for new incoming connections
		FD_SET(sock,&fd);
		int max_fd = sock; // variable containing maximal file descriptor in the set ( needed for the `select` function )

		// adding client sockets to the set to listen for new messages and disconnections
		for(auto &client_socket : clients)
		{
			FD_SET(client_socket.second.GetSocket(),&fd);

			if(client_socket.second.GetSocket() > max_fd)
				max_fd = client_socket.second.GetSocket();
		}

		// waiting for new interactions in file descriptors in the set
		if( select(max_fd+1,&fd,NULL,NULL,NULL) < 0 )
		{
			perror("Select error");
			exit(EXIT_FAILURE);
		}

		// checking new incoming connections ( on the server socket )
		if( FD_ISSET(sock,&fd) )
		{
			// creating a new client
			int new_client;
			int new_id = next_uid++;
			std::string new_login;
			std::string new_group;

			// creating a structure for their address, possibly not necessary
			struct sockaddr_in new_address;
			new_address.sin_family=AF_INET;
			new_address.sin_port=htons(PORT);
			new_address.sin_addr.s_addr=INADDR_ANY;
			int address_len = sizeof(new_address);
			
			// accepting the connections, creating a file descriptor for the new client
			new_client = accept(sock,(struct sockaddr *)&new_address,(socklen_t*)&address_len);
			if(new_client < 0)
			{
				perror("Unable to connect to client socket");
				exit(EXIT_FAILURE);
			}

			// creating an object for the connected client
			clients[new_id] = Client(new_client,new_id);
		}

		std::vector<int> to_erase; // vector containing disconnected clients

		for(auto& client_socket : clients)
		{
			// checking for events on the client socket
			if( FD_ISSET(client_socket.second.GetSocket(),&fd) )
			{
				// reading the message from the socket
				int message_size = read(client_socket.second.GetSocket(),buffer,buffer_size);

				if( message_size <= 0 ) // client disconnected
				{
                    std::string gid;
					for(int index=0;( gid = client_socket.second.GetGID(index) ) != "";index++)
						groups[gid].RemoveUser(client_socket.second.GetUID());
					to_erase.push_back(client_socket.second.GetUID());
				}
				else if( client_socket.second.message.GetLeft() == 0 ) // client sent a new message to his group
				{
					client_socket.second.message.RecieveNew(buffer,message_size,client_socket.second);
				}
				else
				{
					client_socket.second.message.Recieve(buffer,message_size,client_socket.second);
				}
			}
		}

		// removing disconnected clients
		for(auto& dead_client : to_erase)
			clients.erase(dead_client);
	}

	return 0;
}

