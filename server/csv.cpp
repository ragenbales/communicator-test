// Functions for manipulating csv files

#include<fstream>

#include<vector>
#include<string>

#include "csv.h"
#include<iostream>

std::vector<std::string> read_conversation(std::string file_name)
{
	std::ifstream file( file_name, std::ios::in | std::ios::binary );

	std::vector<std::string> messages;
	char info[2*sizeof(int)];

	while(true)
	{
		file.read(info,sizeof(info));

		if(file.gcount()==0)
			break;

		char message[*(int*)(info)+*(int*)(info+sizeof(int))];
		file.read(message,sizeof(message));

		messages.push_back( std::string(info,info+sizeof(info)) +
				std::string(message,message+sizeof(message)) );
	}

	file.close();

	return messages;
}

void add_message(std::string file_name,std::string message)
{
	std::ofstream file( file_name, std::ios::out | std::ios::binary | std::ios::app );

	const char* to_save = message.c_str();

	file.write(to_save,message.size());

	file.close();
}
