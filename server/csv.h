// Functions for manipulating csv files - header

std::vector<std::string> read_conversation(std::string file_name); //takes name of the csv file and returns a vector of all the lines

void add_message(std::string file_name,std::string message); //adds a new message at the end of the csv file with given name
