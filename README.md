# Communicator

## wx compilation

Dependencies:
- wxwidgets 3.1
- patchelf 

To build run `make` in `communicator-test/wx`.

If you don't want to use patchelf, you can alternatively `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:lib`.

## TO DO

1. [ ] local copies of messages
2. [ ] accounts
3. [ ] DMs (probably after 2)
4. [ ] creating  groups (probably after 2)
5. [ ] joining groups (after 2,4)
6. [ ] changing groups (probably after 4)

### Least important, to do when basic functionality is at least decently working

- [ ] audio
- [ ] `make install` using `ldconfig` instead of patchelf
